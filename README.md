## How to start

1. Run mongod
2. Run yarn start (or npm start)
3. Access http://localhost:8080/

## Notes

This app seperates intents submitted in the question field.  It will then look in a mongo database for answers that match the keywords.  If it finds them, it will populate the answer fields with the resulting data and subsequently populate the final reply box.  Upon hitting the submit reply button, it will send the answers to the server and save them in the database.

I spent a little more time on this than the recommended 3 hours, but I finished all the parts and enjoyed the process.  Thanks again for the opportunity!
