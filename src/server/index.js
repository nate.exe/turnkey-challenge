const express = require('express');
const app = express();
const path = require('path');
const jsonParser = require('body-parser').json;
const mongoose = require('mongoose');
const routes = require('./routes');

const port = 8080;

mongoose.connect('mongodb://localhost:27017/QA');
const db = mongoose.connection;

db.on('error', err => {
  console.error(`Error while connecting to DB: ${err.message}`);
});
db.once('open', () => {
  console.log('DB connected successfully!');
});

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  next();
});

app.use(express.static(path.join(__dirname, "../../build")));
app.use(jsonParser());
app.use('/intents', routes);

app.get('/', (req, res) => res.sendFile(path.join(__dirname,'/index.html')));

app.listen(port, () => console.log(`Listening on port ${port}`));