const express = require('express');
const router = express.Router();
const Intent = require('./model').Intent;

// router param
router.param('ID', (req, res, next, id) => {
  Intent.findById(id, (err, intent) => {
    if (err) return next(err);
    if (!intent) {
      err = new Error('Intent not found');
      err.status = 404;
      return next(err);
    }
    req.intent = intent;
    return next();
  });
});

router.param('keywords', (req, res, next, keywords) => {
  Intent.findOne({ keywords: keywords }, (err, intent) => {
    if (err) return next(err);
    if (!intent) {
      err = new Error('Intent not found');
      err.status = 404;
      return next(err);
    }
    req.intent = intent;
    return next(); 
  });
});

// intent routes
router.get('/', (req, res, next) => {
  Intent.find({}).exec((err, intents) => {
    if (err) return next(err);
    res.json(intents);
  });
});

router.post('/', (req, res) => {
  req.body.forEach( x => {
    Intent.findOne({ keywords: x.keywords }, (err, intent) => {
      if (!intent) {
        intent = new Intent(x);
      } else {
        intent.set({ answer: x.answer });
      }
      intent.save((err, intent) => {
        if (err) return next(err);
      });
    })
  });
  res.status(201);
});

router.get('/:ID', (req, res) => {
  res.json(req.intent);
});

router.get('/search/:keywords', (req, res) => {
  res.json(req.intent);
})

module.exports = router;