const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const IntentSchema = new Schema({
  keywords: String,
  answer: String
});

const Intent = mongoose.model('Intent', IntentSchema);

module.exports.Intent = Intent;