import React from 'react';
import Intent from './Intent.js';

const AnswerForm = ({ intents, handleAnswerChange, handleReplySubmit, replyText, handleReplyChange }) => (
  <div>
    {intents.map( (intent, i) => <Intent intent={intent} handleAnswerChange={handleAnswerChange(i)} key={i}/>)}
    <div className="divider"></div>
    <p className="label">REPLY</p>
      <textarea rows="10" value={replyText} onChange={handleReplyChange}></textarea>
    <button onClick={handleReplySubmit}>Submit</button>
  </div>
);

export default AnswerForm;