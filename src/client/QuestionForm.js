import React from 'react';

const QuestionForm = ({ questionText, handleQuestionChange, handleQuestionSubmit }) => (
  <form action="" onSubmit={handleQuestionSubmit}>
    <p className="label">ASK A QUESTION</p>
    <textarea name="" id="" cols="30" rows="10" value={questionText} onChange={handleQuestionChange}></textarea>
    <button type="submit">Submit</button>
    <div className="divider"></div>
  </form>
)

export default QuestionForm;