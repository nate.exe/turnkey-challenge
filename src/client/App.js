import React, { Component } from 'react';
import './App.css';
import keyword_extractor from 'keyword-extractor';
import QuestionForm from './QuestionForm';
import AnswerForm from './AnswerForm';

class App extends Component {
  state = {
    questionText: 'Do you have wifi? Do you have a washer?',
    replyText: '',
    intents: []
  }

  handleQuestionChange = (e) => {
    console.log(this.state.questionText);
    this.setState({questionText: e.target.value});
  }

  handleQuestionSubmit = (e) => {
    e.preventDefault();
    const questions = this.state.questionText.match(/\S+.+?[?!.]/g);
    const intents = questions.map( question => {
      const keywords = keyword_extractor.extract(question).sort().join('-');
      return {
        question: question,
        keywords: keywords,
        answer: ''
      }
    });

    this.setState({ intents }, this.findAnswers)
  }

  findAnswers = () => {
    this.state.intents.forEach( (intent, i) => {
      const intents = this.state.intents.slice();
      fetch(`http://localhost:8080/intents/search/${intent.keywords}`)
      .then(data => data.json())
      .then(data => {
        intents[i].answer = data.answer ? data.answer : '';
        this.setState({ intents }, this.updateReplyText)
      })
      .catch(err => console.log(err))
      
    })
  }

  handleReplyChange = (e) => {
    this.setState({replyText: e.target.value});
  }

  handleAnswerChange = (i) => {
    return (e) => {
      let newIntents = this.state.intents.slice();
      newIntents[i].answer = e.target.value;
      this.setState({intents: newIntents}, this.updateReplyText);
    }
  }

  updateReplyText = () => {
    const replyText = this.state.intents.reduce( (acc, intent, i) => {
      const line = `${intent.question}\n${intent.answer}`
      return i === 0 ? acc + line : acc + '\n\n' + line;
    }, '');
    this.setState({ replyText });
  }

  handleReplySubmit = (e) => {
    this.updateModel();
  }

  updateModel = () => {
    const data = this.state.intents.map( intent => {
      return {
        keywords: intent.keywords, 
        answer: intent.answer
      }
    });
    fetch('http://localhost:8080/intents/', {
      method: "POST",
      headers: {
          "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
    .then(response => response.json())
    .then(alert('Reply sent'))
    .catch(error => console.error(`Fetch Error =\n`, error));
  }

  render() {
    return (
      <div className="App">
        <QuestionForm 
          questionText={this.state.questionText} 
          handleQuestionChange={this.handleQuestionChange} 
          handleQuestionSubmit={this.handleQuestionSubmit} 
        />
        <AnswerForm 
          intents={this.state.intents} 
          handleAnswerChange={this.handleAnswerChange} 
          replyText={this.state.replyText} 
          handleReplyChange={this.handleReplyChange} 
          handleReplySubmit={this.handleReplySubmit}
        />
      </div>
    );
  }
}

export default App;
