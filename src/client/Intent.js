import React from 'react';

const Intent = ( { intent, handleAnswerChange } ) => (
  <div className="intent">
    <p className="label">QUESTION</p>
    <p>{intent.question}</p>
    <p className="label">KEYWORDS</p>
    <p>{intent.keywords}</p>
    <p className="label">ANSWER</p>
    <input type="text" value={intent.answer} onChange={handleAnswerChange}/>
  </div>
);

export default Intent;